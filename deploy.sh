. ./.env

docker rm -f ${MEM_APPWEB} ${MEM_APP} ${MEM_DB}

cp -a sql ./build/db/
docker build -t ${MEM_DB}:latest ./build/db/
imageid=$(docker images ${MEM_DB} | grep '<none>' | tail -n 1 | awk -F ' ' '{print $3}')
docker rmi $imageid
rm -rf ./build/db/sql


cp -a munajatemaqbool ./build/app/
cp -a manage.py requirements.txt ./build/app/
docker build -t ${MEM_APP}:latest ./build/app/
imageid=$(docker images ${MEM_APP} | grep '<none>' | tail -n 1 | awk -F ' ' '{print $3}')
docker rmi $imageid
rm -rf ./build/app/munajatemaqbool
rm -f ./build/app/manage.py
rm -f ./build/app/requirements.txt


docker build -t ${MEM_APPWEB}:latest ./build/web/
imageid=$(docker images ${MEM_APPWEB} | grep '<none>' | tail -n 1 | awk -F ' ' '{print $3}')
docker rmi $imageid


imageid=$(docker images | grep '<none>' | tail -n 1 | awk -F ' ' '{print $3}')
docker rmi $imageid

docker-compose --file docker-compose-production.yml up -d
