#!/bin/bash
set -e

if [ -n "$DNSNAME" ]
then
    echo "setting $DNSNAME in /etc/nginx/conf.d/default.conf"
    sed "s/dnsname/$DNSNAME/" -i /etc/nginx/conf.d/default.conf
fi

nginx -g 'daemon off;'
