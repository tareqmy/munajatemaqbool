#!/usr/bin/env bash

if [ -n "$DB_HOST" ]
then
    sed "s/host = .*/host = $DB_HOST/" -i munajatemaqbool/conf/database.ini
fi

if [ -n "$DB_USER" ]
then
    sed "s/user = .*/user = $DB_USER/" -i munajatemaqbool/conf/database.ini
fi

if [ -n "$DB_PASSWORD" ]
then
    sed "s/password = .*/password = $DB_PASSWORD/" -i munajatemaqbool/conf/database.ini
fi

python manage.py