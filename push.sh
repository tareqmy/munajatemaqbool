. ./.env


echo "Rebuilding $MEM_DB image..."
cp -a sql ./build/db/
docker build -t tareqmy.vantageip.com:5000/${MEM_DB}:latest ./build/db/
docker push tareqmy.vantageip.com:5000/${MEM_DB}:latest
imageid=$(docker images tareqmy.vantageip.com:5000/${MEM_DB} | grep '<none>' | tail -n 1 | awk -F ' ' '{print $3}')
docker rmi $imageid
rm -rf ./build/db/sql



echo "Rebuilding $MEM_APP image..."
cp -a munajatemaqbool ./build/app/
cp -a manage.py requirements.txt ./build/app/
docker build -t tareqmy.vantageip.com:5000/${MEM_APP}:latest ./build/app/
docker push tareqmy.vantageip.com:5000/${MEM_APP}:latest
imageid=$(docker images tareqmy.vantageip.com:5000/${MEM_APP} | grep '<none>' | tail -n 1 | awk -F ' ' '{print $3}')
docker rmi $imageid
rm -rf ./build/app/munajatemaqbool
rm -f ./build/app/manage.py
rm -f ./build/app/requirements.txt


echo "Rebuilding $MEM_APPWEB image..."
docker build -t tareqmy.vantageip.com:5000/${MEM_APPWEB}:latest ./build/web/
docker push tareqmy.vantageip.com:5000/${MEM_APPWEB}:latest
imageid=$(docker images tareqmy.vantageip.com:5000/${MEM_APPWEB} | grep '<none>' | tail -n 1 | awk -F ' ' '{print $3}')
docker rmi $imageid



imageid=$(docker images | grep '<none>' | tail -n 1 | awk -F ' ' '{print $3}')
docker rmi $imageid

docker rmi tareqmy.vantageip.com:5000/$MEM_DB:latest
docker rmi tareqmy.vantageip.com:5000/$MEM_APP:latest
docker rmi tareqmy.vantageip.com:5000/$MEM_APPWEB:latest
