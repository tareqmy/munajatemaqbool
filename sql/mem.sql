--
-- Name: dua; Type: TABLE; Schema: public; Owner: mem
--

CREATE TABLE dua (
    id integer NOT NULL,
    number integer,
    arabic text NOT NULL,
    english text NOT NULL,
    bengali text NOT NULL,
    tags text
);


ALTER TABLE dua OWNER TO mem;

--
-- Name: dua_id_seq; Type: SEQUENCE; Schema: public; Owner: mem
--

CREATE SEQUENCE dua_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dua_id_seq OWNER TO mem;

--
-- Name: dua_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mem
--

ALTER SEQUENCE dua_id_seq OWNED BY dua.id;


--
-- Name: misc; Type: TABLE; Schema: public; Owner: mem
--

CREATE TABLE misc (
    id integer NOT NULL,
    arabic text NOT NULL,
    english text NOT NULL,
    bengali text NOT NULL,
    type text NOT NULL
);


ALTER TABLE misc OWNER TO mem;

--
-- Name: misc_id_seq; Type: SEQUENCE; Schema: public; Owner: mem
--

CREATE SEQUENCE misc_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE misc_id_seq OWNER TO mem;

--
-- Name: misc_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mem
--

ALTER SEQUENCE misc_id_seq OWNED BY misc.id;


--
-- Name: dua id; Type: DEFAULT; Schema: public; Owner: mem
--

ALTER TABLE ONLY dua ALTER COLUMN id SET DEFAULT nextval('dua_id_seq'::regclass);


--
-- Name: misc id; Type: DEFAULT; Schema: public; Owner: mem
--

ALTER TABLE ONLY misc ALTER COLUMN id SET DEFAULT nextval('misc_id_seq'::regclass);