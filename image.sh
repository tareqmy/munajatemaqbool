. ./.env


cp -a sql ./build/db/
docker build -t ${MEM_DB}:latest ./build/db/
imageid=$(docker images ${MEM_DB} | grep '<none>' | tail -n 1 | awk -F ' ' '{print $3}')
docker rmi $imageid
rm -rf ./build/db/sql


cp -a munajatemaqbool ./build/app/
cp -a manage.py requirements.txt ./build/app/
docker build -t ${MEM_APP}:latest ./build/app/
imageid=$(docker images ${MEM_APP} | grep '<none>' | tail -n 1 | awk -F ' ' '{print $3}')
docker rmi $imageid
rm -rf ./build/app/munajatemaqbool
rm -f ./build/app/manage.py
rm -f ./build/app/requirements.txt


cp -a memappclient ./build/client/
docker build -t ${MEM_APPCLIENT}:latest ./build/client/
imageid=$(docker images ${MEM_APPCLIENT} | grep '<none>' | tail -n 1 | awk -F ' ' '{print $3}')
docker rmi $imageid
rm -rf ./build/client/memappclient


imageid=$(docker images | grep '<none>' | tail -n 1 | awk -F ' ' '{print $3}')
docker rmi $imageid
