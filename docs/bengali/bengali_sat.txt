আয় আল্লাহ্! আমাদিগকে দান কর দুনিয়ার ভাল অবস্থা এবং আখেরাতেও ভাল অবস্থা এবং আমাদিগকে দোযখের আযাব হইতে বাঁচাও।
আয় আল্লাহ্! আমাদিগকে ছবর দান কর, আর আমাদিগকে মজবুত রাখ (তোমার দ্বীনের উপর) এবং কাফেরদের মোকাবেলায় জয়ী কর।
আয় আল্লাহ্! আমাদের ভুলত্রুটী ধরিও না। আয় আল্লাহ্! আমাদের উপর জারী করিও না কঠোর আইন পুর্ববর্তী উম্মতগণের ন্যায়। আয় আল্লাহ্! আমাদের শক্তির বাহিরে কোন হুকুম জারী করিও না। এবং আমাদের অন্যায় ক্ষমা কর এবং আমাদের ত্রুটি মার্জনা কর এবং আমাদের প্রতি দয়ার নজর রাখ; তুমিই আমাদের একমাত্র মালিক, অতএব, তুমি আমাদিগকে কাফেরদের মোকাবেলায় জয়ী কর।
আয় আল্লাহ্! একবার তুমি আমাদিগকে হেদায়েত দান করিয়াছ, এখন আবার আমাদেরে বিচলিত হইতে দিও না। এবং তুমি সবচেয়ে বড় দাতা, তোমার নিকট হইতে আমাদিগকে রহমত দান কর।
আয় আল্লাহ্! আমরা ঈমান আনিয়াছি; আমাদের সব গোনাহ মাফ করিয়া আমাদেরে দোযখের আযাব হইতে রক্ষা কর।
হে আমাদের প্রভু! তুমি জগতকে অযথা সৃষ্টি কর নাই, (তোমার আজ্ঞাবহ দাস বানাইবার জন্য সৃষ্টি করিয়াছ;) অযথা কাজ করা হইতে তুমি পবিত্র। অতএব, আমাদিগকে (তোমার দাস বানাইয়া) দোযখের আযাব হইতে বাঁচাইয়া রাখিও। হে মহান! তুমি যাহাদের দোজখে ফেলিবে তাহাদের লাঞ্ছনা-গঞ্জনার সীমা নাই এবং এদের জন্য কোন সহায়ও নাই। আয় আল্লাহ্! আমরা একজন ঘোষণাকারীকে ঘোষণা করিতে শুনিয়াছি যে, "তোমরা তোমাদের খোদার উপর ঈমান আন" ইহা শুনিয়া আমরা ঈমান আনিয়াছি। অতএব, হে খোদা! আমাদের সব গোনাহ মাফ করিয়া দাও এবং যাহা কিছু অন্যায়-ত্রুটি আমাদের আছে সব দূর করিয়া দাও। এবং নেক লোকদের দলভুক্ত করিয়া আমাদের মৃত্যু দিও এবং হে আল্লাহ্! আমাদিগকে দান করিও যাহা দান করার ওয়াদা করিয়াছ তোমার পয়গম্বরদের মধ্যবর্তীতায় এবং কিয়ামতের দিন আমাদিগকে অপমান করিও না, নিশ্চয়ই তুমি কখনও খেলাফ কর না তোমার ওয়াদা।
হে আল্লাহ্! আমরা নিজেরাই নিজেদের উপর অত্যাচার করিয়াছি, এখন যদি তুমি ক্ষমা না কর, দয়া না কর, তবে আমাদের সর্বনাশ হইবে।
আয় আল্লাহ্! আমাদিগকে সহ্যগুণ দান কর এবং ঈমানের সহিত মৃত্যু দিও।
তুমিই আমাদের একমাত্র সহায়; অতএব, আমাদিগকে ক্ষমা কর এবং দয়া কর; তুমিই সর্বোত্তম ক্ষমাকারী।
আয় আল্লাহ্! তুমি আমাদেরে যুলুমের স্থান বানাইয়া অধিক পথভ্রষ্ট হইতে দিও না। অত্যাচারীদিগকে এবং নিজ দয়াগুণে আমাদিগকে কাফেরদের হাত হইতে মুক্তি দান কর।
হে আকাশ ও যমীনের স্রষ্টা! তুমি আমার একমাত্র সহায় দুনিয়া ও আখেরাতে। আমাকে ঈমানের সহিত মৃত্যু দিও এবং মিলাইয়া রাখিও নেক লোকদের সহিত।
আয় আল্লাহ্! খাঁটি মুসল্লি বানাও আমাকে এবং আমার বংশধরগণকে! আয় আল্লাহ্! আমার দোয়া কবুল কর। আয় আল্লাহ্! ক্ষমা করিয়া দিও আমাকে এবং আমার পিতা-মাতা ও সমস্ত মু'মিনগণকে কেয়ামতের হিসাবের দিন।
হে আল্লাহ্! আমার পিতা-মাতার উপর রহমত নাযিল কর, যেমন তাঁহারা আমাকে শিশুকালে লালন-পালন করিয়াছেন।
আয় আল্লাহ্! আমাকে যেখানে নাও ভালভাবে নিও এবং যেখান হইতে নাও ভালভাবে নিও এবং নিযুক্ত কর তোমার পক্ষ হইতে আমার জন্য এক শক্তিশালী সাহায্যকারী।
আয় আল্লাহ্! তোমার নিকট হইতে আমাদিগকে রহমত দান কর এবং সুবন্দোবস্ত করিয়া দাও আমাদের সব কাজের।
আয় আল্লাহ্! আমার অন্তঃকরণ প্রশস্ত করিয়া দাও এবং আমার কাজ সহজ করিয়া দাও, আমার জিহবার গিরা (জড়তা) খুলিয়া দাও, যাহাতে লোকেরা আমার কথা সহজে বুঝিতে পারে।
আয় আল্লাহ্! আমার এল্ম বাড়াইয়া দাও।
হে মা'বুদ! আমাকে রোগে ধরিয়াছে; তুমি সর্বাধিক দয়ালু।
আয় আল্লাহ্! আমাকে একা ছাড়িও না; তুমি সর্বোত্তম উত্তরাধিকারী।
আয় আল্লাহ্! আমাকে বরকতের ও রহমতের ঠিকানায় পৌঁছাইয়া দাও, তুমিই সর্বোত্তম পৌঁছানেওয়ালা।
আয় আল্লাহ্! আমি তোমার আশ্রয় গ্রহণ করি - শয়তান যেন আমার উপর তাছির করিতে না পারে এবং আমি তোমারই আশ্রয় গ্রহণ করি - আয় আল্লাহ্! তাহারা যেন আমার কাছেও আসিতে না পারে।
আয় আল্লাহ্! আমরা ঈমান আনিয়াছি, অতএব, আমাদের সব গোনাহ মাফ করিয়া দাও এবং আমাদের উপর মেহেরবানী কর, তুমি সর্বোত্তম মেহেরবান।
আয় আল্লাহ্! আমাদের হইতে সরাইয়া দিও দোযখের আযাব। নিশ্চয় দোযখের আযাব সর্বনাশ সাধনকারী।
আয় আল্লাহ্! আমাদিগকে দান কর এমন স্ত্রী, পুত্র-কন্যা যেন তাহাদের কারণে আমাদের চক্ষু শীতল হয় এবং আমাদের (সর্দার বানাও তো) মুত্তাকীদের সর্দার বানাও।
আয় মা'বুদ! আমাকে দান কর তোমার বিশেষ রহমতভান্ডার হইতে মঙ্গলময় নেককার সন্তান; নিশ্চয় তুমি সকল দোয়া শ্রবণকারী।
আয় আল্লাহ্! তুমি আমাকে তাওফিক দান কর তোমার ঐ সব নেয়ামতের শোকর আদায় করিবার যে সব নেয়ামত তুমি দান করিয়াছ আমাকে এবং আমার পিতা-মাতাকে এবং আমাকে তাওফিক দান কর এরূপ নেক আমল করিবার যাহা তুমি পছন্দ কর এবং তোমার দয়াগুণে আমাকে দলভুক্ত করিয়া তোমার নেক বান্দাগণের।
আয় আল্লাহ্! তুমি যাহা কিছু আমাকে দান কর তাহাই ভাল এবং তাহারই আমি মুখাপেক্ষী।
আয় আল্লাহ্! আমাকে জয়যুক্ত কর। ফেৎনা-ফাসাদকারীদের মোকাবেলায়।
আয় আল্লাহ্! সর্বব্যপী তোমার রহমত এবং তুমি সর্বজ্ঞ; অতএব, ক্ষমা কর তাহাদেরে যাহারা তওবা করিয়া গ্রহণ করিয়াছে তোমার দ্বীনের পথ এবং তাহাদের দোযখের আযাব হইতে বাঁচাও। আয় আল্লাহ্! স্থান দান কর তাহাদেরে তোমার ওয়াদাকৃত চিরস্থায়ী বেহেশ্তের মধ্যে এবং তাহাদেরেও যাহারা নেককার হইয়াছে, তাহাদের বাপ, দাদা, স্ত্রী ও পুত্র-কণ্যাগণের মধ্যে; নিশ্চয়ই তুমি সর্বশক্তিমান সর্বক্ষমতাশালী। তাহাদের সব কষ্ট হইতে বাঁচাইয়া লও; তুমি যাহাকে বাঁচাইয়া নিলে সব কষ্ট হইতে কেয়ামতের দিন, সে-ই প্রকৃত প্রস্তাবে তোমার রহমত পাইল এবং ইহাই (জীবনের চরম উদ্দেশ্য সাধন ও ) বড় সার্থকতা।
আয় আল্লাহ্! আমার আল-আওলাদের মধ্যে ঈমানদার পরহেজগার রাখিও; আমি তওবা করিয়া তোমার দিকে রুজু হইতেছি এবং তোমার ফরমাবরদারী গ্রহণ করিয়াছি।
আমি পরাজিত হইতেছি, আমার সহায়তা কর এবং প্রতিশোধ লও।
আয় আল্লাহ্! মাফ করিয়া দাও আমাদেরে এবং আমাদের যে সব মুসল্মান ভাইগণ ঈমানের সঙ্গে গুযরিয়া গিয়াছেন তাহাদেরে এবং থাকিতে দিও না আমাদের দীলে বিন্দুমাত্র কীনা কোন ঈমানদারের প্রতি; আয় আল্লাহ্! তুমি অতি মেহেরবান অতি দয়ালু।
আয় আল্লাহ্! আমরা তোমারই উপর ভরসা করিতেছি এবং তোমারই দিকে রুজু হইতেছি এবং অবশেষে তোমারই নিকট ফিরিয়া আসিতে হইবে। আয় আল্লাহ্! আমাদেরে কাফেরদের দ্বারা উৎপীড়িত হইতে দিও না। আয় আল্লাহ্! আমাদের মাফ করিয়া দাও; হে প্রভু! নিশ্চয় তুমি সর্বশক্তিমান সর্ব ক্ষমতাবান।
আয় আল্লাহ্! আমাদের জন্য আমাদের নূরকে পূর্ণ করিয়া দাও এবং আমাদেরে মাফ করিয়া দিও; নিশ্চয় তুমি সব কিছু করিতে পার।
আয় আল্লাহ্! সমস্ত গোনাহ-খাতা মাফ করিয়া দাও আমাকে এবং আমার পিতা-মাতাকে এবং যে ঈমানের সহিত আমার ঘরে ঢুকিয়াছে তাহাকে এবং অন্য সমস্ত ঈমানদার পুরুষ এবং ঈমানদার স্ত্রীলোকগণকে।
আয় আল্লাহ্! আমার সমস্ত গোনাহ ধুইয়া দাও - বরফের এবং শিলার পানি দ্বারা এবং আমার দীল্কে গোনাহসমূহ হইতে সেইরূপ পরিস্কার করিয়া দাও যেরূপ সাদা কাপড় ময়লা হইতে ধুইয়া পরিস্কার করা হয় এবং দূরে রাখ আমাকে গোনাহের কাজ হইতে, যেরূপ দূরে আছে মাশরিক (পূর্ব) হইতে মাগরিব (পশ্চিম)।
আয় আল্লাহ্! তুমি আমার নফসকে দান কর পরহেজগারী এবং আমার নফসে এছলাহ করিয়া দাও, তুমিই সর্বোত্তম এছলাহকারী; তুমি আমার নফসের মালিক, তুমি উহার মাওলা।
আয় আল্লাহ্! আমরা তোমার নিকট প্রার্থনা করি ঐ সব ভাল জিনিস যে সবের দরখাস্ত করিয়াছেন তোমার নিকট তোমার পেয়ারা নবী মুহাম্মদ সাল্লাল্লাহু আলাইহি ওয়া সাল্লাম।
আয় আল্লাহ্! আমরা তোমার নিকট এমন সব আমলের তাওফিক চাই, যাহাতে তোমার নিকট মাফি এবং নাজাত পাই, এবং সব গোনাহের কাজ হইতে বাঁচিয়া থাকিতে পারি এবং সহজে প্রচুর পরিমাণে প্রত্যেক সওয়াবের কাজ করিতে পারি এবং বেহেশ্ত লাভ করিতে পারি ও দোযখ হইতে মুক্তি পাই।
আয় আল্লাহ্! তোমার নিকট এমন এল্ম চাই যাহা কাজে আসে।
আয় আল্লাহ্! আমাকে মাফ করিয়া দাও আমার সমস্ত গোনাহ এবং যাহা কিছু অন্যায় ইচ্ছা বা ইনিচ্ছায় করিয়া থাকি।
আয় আল্লাহ্! আমাকে মাফ করিয়া দাও আমার সব ত্রুটি-অন্যায় যাহা কিছু না জানিয়া করিয়াছি এবং স্বীয় কার্যাবলীতে যাহা কিছু সীমা অতিক্রম করিয়াছি এবং যাহা কিছু আমি জানি না - তুমি আমার চেয়ে বেশী জান।
আয় আল্লাহ্! আমাকে মাফ করিয়া দাও যাহা কিছু অন্যায় আমি ইচ্ছাপূর্বক করিয়াছি বা হাসি তামাশায় করিয়াছি।
আয় আল্লাহ্! সমস্ত দীল তোমার হাতে; তুমি আমাদের দীল্কে তোমার ফরমাবরদারীর মধ্যে লাগাইয়া রাখ।
আয় আল্লাহ্! আমাকে হেদায়েত দান কর এবং সোজা লক্ষ্যস্থলে পৌঁছিতে মজবুত রাখ।
আয় আল্লাহ্! আমি তোমার নিকট হেদায়েত চাই এবং বদ কাজ হইতে বাঁচাইয়া থাকিতে তাকওয়া-পরহেজগারী চাই এবং পরের বৌ-ঝিয়ের দিকে বা পরের টাকা-পয়সার দিকে ফিরিয়াও যেন না চাই এবং নিজের আর্থিক অবস্থায় নিজে যেন সন্তুষ্ট থাকিতে পারি।
আয় আল্লাহ্! দুরস্ত করিয়া দাও আমার দ্বীন। দ্বীনই আমার আসল সম্বল এবং দুরস্ত করিয়া দাও আমার দুনিয়া; যে দুনিয়াতে আমার জীবিকা এবং দুরস্ত করিয়া আমার আখেরাত; সে আখেরাতেই আমার আসল ঠিকানা ও ফিরিয়া যাইবার স্থান এবং হায়াতকে উপায় বানাইয়া দাও সব রকমের নেক আমল বেশী হইবার এবং মৃত্যুকে উপায় বানাও সব কষ্ট হইতে শান্তি লাভের।
আয় আল্লাহ্! মাফ কর আমাকে এবং রহমত দান কর, আমাকে সাস্থ্য দান কর, আমাকে হালাল রুজি দান কর। আয় আল্লাহ্! আমি তোমার আশ্রয় গ্রহণ করিতেছি; আমাকে বাঁচাও-আমি যেন অকর্ম্মন্য না হই, আমি যেন অলস না হই, আমি যেন ভীরু কাপুরুষ না হই, আমি যেন সম্পূর্ণ অচল বৃদ্ধ না হই, আমি যেন ঋণের বোঝার চাপে না পড়ি, আমি যেন গোনাহের কাজের কাছেও না যাই এবং দোযখের আযাব হইতে বাঁচাও। এবং দোযখের আগুনে জ্বলা হইতে বাঁচাও, কবরের পরীক্ষার সংকট হইতে বাঁচাও; কবরের আযাব হইতে বাঁচাও। আর আমি যেন মালদারীর পরীক্ষায় অনুত্তীর্ণ না হই এবং দরিদ্রতার পরীক্ষায় অকৃতকার্য না হই। আর ধোকায় যেন না পড়ি দাজ্জালের ফেতনার। আর জীবিত অবস্থার ফেতনা হইতে এবং মৃত্যু সময়কার ফেৎনা হইতে আমাকে বাঁচাও এবং আমার দীল যেন শক্ত না হয়। আর আমাকে গাফলত হইতে বাঁচাও; দরিদ্রতা ও অভাব-অনটন হইতে বাঁচাও মানহীনতা, ইতরতা এবং কুফরী-ফাসেকী হইতে বাঁচাও। জেদাজেদী, দলাদলী ও রিয়াকারী হইতে বাঁচাও। বধিরতা এবং বাকশক্তি রহিত হওয়া, উন্মত্ততা, কুষ্ঠরোগ এবং অন্যান্য সব খারাপ রোগ হইতে বাঁচাও। ঋণ-ভার এবং চিন্তা-ভাবনা হইতে এবং কৃপণতা হইতে বাঁচাও এবং লোকের চাপ ও জুলুম হইতে বাঁচাও। যেই বয়সে অর্কমা হইয়া পড়ি সেই বয়স হইতে বাঁচাও। দুনিয়ার ফেৎনা-ফাসাদ হইতে বাঁচাও এবং যেই এল্মের মধ্যে কোন উপকার নাই সেই এল্ম হইতে বাঁচাও, যে দীল নরম না হয় সেই দীল হইতে বাঁচাও। যেই নফস তৃপ্ত না হয় সেই নফস হইতে বাঁচাও, যে দোয়া কবুল না হয় সেই দোয়া হইতে বাঁচাও।