import os

from munajatemaqbool import application

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 7000))
    application.run(host='0.0.0.0', port=port)
