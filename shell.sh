. ./.env

if [ $# -eq 1 ]
then

    if [ "$1" == "db" ]; then
        echo "shell for $MEM_DB"
        docker exec -it $MEM_DB /bin/bash
        exit 0;
    elif [ "$1" == "web" ]; then
        echo "shell for $MEM_APPWEB"
        docker exec -it $MEM_APPWEB /bin/bash
        exit 0;
    else
        echo "shell for $MEM_APPCLIENT"
        docker exec -it $MEM_APPCLIENT /bin/sh
        exit 0;
    fi
fi
echo "shell for $MEM_APP"
docker exec -it $MEM_APP /bin/bash
