from flask import Flask, make_response, jsonify
from flask import render_template
from flask_cors import CORS

from munajatemaqbool.api import api_blueprint
from munajatemaqbool.extensions import db
from munajatemaqbool.models.misc import Misc


def create_app():
    application = Flask(__name__)
    cors = CORS(application, resources={r"/api/*": {"origins": "*"}})
    application.config.from_pyfile('conf/config.py')
    register_extensions(application)
    register_blueprints(application)
    return application


def register_extensions(application):
    db.init_app(application)


def register_blueprints(app):
    app.register_blueprint(api_blueprint)


# this should be called from manage.py but how to add the errorhandlers then???
# https://github.com/joshfriend/flask-restful-demo/blob/master/manage.py
application = create_app()


@application.route('/')
def login():
    return make_response(jsonify({'error': 'Not found'}), 404)


@application.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@application.errorhandler(500)
def internal_server_error(error):
    return make_response(jsonify({'error': 'Internal Server Error'}), 500)


from . import extensions, helpers
from .conf import config, dbconfig
from .models import misc, dua
