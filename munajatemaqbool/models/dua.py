from munajatemaqbool.extensions import db


class Dua(db.Model):
    """Model for the dua table"""
    __tablename__ = 'dua'

    id = db.Column(db.Integer, primary_key=True)
    number = db.Column(db.Integer, nullable=False)
    arabic = db.Column(db.Text, nullable=False)
    english = db.Column(db.Text, nullable=False)
    bengali = db.Column(db.Text, nullable=False)
    tags = db.Column(db.Text, nullable=False)

    def __init__(self, number, arabic, english, bengali, tags):
        self.number = number
        self.arabic = arabic
        self.english = english
        self.bengali = bengali
        self.tags = tags

    def __repr__(self):
        return '<dua(number={self.number}, tags={self.tags}, english={self.english!r})>'.format(self=self)

    def json(self):
        items = {
            'id': self.id,
            'number': self.number,
            'arabic': self.arabic,
            'english': self.english,
            'bengali': self.bengali,
            'tags': self.tags,
        }
        return items

    @classmethod
    def get_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def get_by_tags(cls, tags):
        return cls.query.filter_by(tags=tags).first()

    @classmethod
    def get_all_tags(cls):
        return cls.query.with_entities(Dua.tags).distinct(Dua.tags).all()
