from munajatemaqbool.extensions import db


class Misc(db.Model):
    """Model for the misc table"""
    __tablename__ = 'misc'

    id = db.Column(db.Integer, primary_key=True)
    arabic = db.Column(db.Text, nullable=False)
    english = db.Column(db.Text, nullable=False)
    bengali = db.Column(db.Text, nullable=False)
    type = db.Column(db.Text, nullable=False)

    def __init__(self, arabic, english, bengali, type):
        self.arabic = arabic
        self.english = english
        self.bengali = bengali
        self.type = type

    def __repr__(self):
        return '<misc(type={self.type}, english={self.english!r})>'.format(self=self)

    def json(self):
        items = {
            'id': self.id,
            'arabic': self.arabic,
            'english': self.english,
            'bengali': self.bengali,
            'type': self.type,
        }
        return items

    @classmethod
    def get_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def get_by_type(cls, type):
        return cls.query.filter_by(type=type).first()
