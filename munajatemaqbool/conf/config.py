from munajatemaqbool.conf.dbconfig import getpgdbconfig

DEBUG = False
SECRET_KEY = '63D61899173DD8CAFAC8C49D219F6'
SQLALCHEMY_DATABASE_URI = getpgdbconfig()
SQLALCHEMY_TRACK_MODIFICATIONS = False
