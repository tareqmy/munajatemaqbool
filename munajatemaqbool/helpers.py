import functools

from flask import request, url_for


def paginate(max_per_page=4):
    def decorator(func):
        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            page = request.args.get('page', 1, type=int)
            size = min(request.args.get('size', max_per_page,
                                        type=int),
                       max_per_page)

            query = func(*args, **kwargs)
            p = query.order_by('id').paginate(page, size)

            meta = {
                'page': page,
                'size': size,
                'total': p.total,
                'pages': p.pages,
            }

            links = {}
            if p.has_next:
                links['next'] = url_for(request.endpoint, page=p.next_num,
                                        size=size, **kwargs)
            if p.has_prev:
                links['prev'] = url_for(request.endpoint, page=p.prev_num,
                                        size=size, **kwargs)
            links['first'] = url_for(request.endpoint, page=1,
                                     size=size, **kwargs)
            links['last'] = url_for(request.endpoint, page=p.pages,
                                    size=size, **kwargs)

            meta['links'] = links
            result = {
                'items': p.items,
                'meta': meta
            }

            return result, 200

        return wrapped

    return decorator
