from flask import abort
from flask_restful import Resource, marshal_with, fields

from munajatemaqbool.api import api, meta_fields
from munajatemaqbool.helpers import paginate
from munajatemaqbool.models.misc import Misc

# Marshaled field definitions for misc objects
misc_fields = {
    'id': fields.Integer,
    'arabic': fields.String,
    'english': fields.String,
    'bengali': fields.String,
    'type': fields.String,
}

# Marshaled field definitions for collections of user objects
misc_collection_fields = {
    'items': fields.List(fields.Nested(misc_fields)),
    'meta': fields.Nested(meta_fields),
}


class MiscResource(Resource):
    @marshal_with(misc_fields)
    def get(self, id=1):
        misc = Misc.get_by_id(id)

        if not misc:
            abort(404)

        return misc


class MiscCollectionResource(Resource):
    @marshal_with(misc_collection_fields)
    @paginate()
    def get(self, type=None):
        miscs = None
        if type is not None:
            miscs = Misc.query.filter_by(type=type)
        else:
            miscs = Misc.query
        return miscs


api.add_resource(MiscResource, '/misc/<int:id>')
api.add_resource(MiscCollectionResource, '/misc', '/misc/<string:type>')
