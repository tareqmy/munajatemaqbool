from flask import abort
from flask_restful import Resource, marshal_with, fields

from munajatemaqbool.api import api, meta_fields
from munajatemaqbool.helpers import paginate
from munajatemaqbool.models.dua import Dua

# Marshaled field definitions for dua objects
dua_fields = {
    'id': fields.Integer,
    'number': fields.Integer,
    'arabic': fields.String,
    'english': fields.String,
    'bengali': fields.String,
    'tags': fields.String,
}

# Marshaled field definitions for collections of dua objects
dua_collection_fields = {
    'items': fields.List(fields.Nested(dua_fields)),
    'meta': fields.Nested(meta_fields),
}


class DuaResource(Resource):
    @marshal_with(dua_fields)
    def get(self, id=1):
        dua = Dua.get_by_id(id)

        if not dua:
            abort(404)

        return dua


class DuaDaysResource(Resource):
    def get(self):
        dua_days = Dua.get_all_tags()
        dua_json = []
        for dua in dua_days:
            dua_json.append(dua.tags)
        return dua_json


class DuaCollectionResource(Resource):
    @marshal_with(dua_collection_fields)
    @paginate()
    def get(self, tags=None):
        duas = None
        if tags is not None:
            duas = Dua.query.filter_by(tags=tags)
        else:
            duas = Dua.query
        return duas


api.add_resource(DuaResource, '/dua/<int:id>')
api.add_resource(DuaDaysResource, '/dua/days')
api.add_resource(DuaCollectionResource, '/dua', '/dua/<string:tags>')
